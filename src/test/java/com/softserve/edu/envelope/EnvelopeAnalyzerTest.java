package com.softserve.edu.envelope;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alin- on 13.12.2017.
 */
public class EnvelopeAnalyzerTest {

    private Envelope envelope1;
    private Envelope envelope2;

    @Test
    public void analyzeEnvelopesTestWithNullValues(){
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void alalyzeEnvelopesTestWithFirstNullValue(){
        envelope2 = new Envelope(4,6);
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void alalyzeEnvelopesTestWithLastNullValue(){
        envelope1 = new Envelope(4,6);
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void analyzeEnvelopesTestWithPositiveFloatValues(){
        envelope1 = new Envelope(7.5f,10.4f);
        envelope2 = new Envelope(3f,4f);
        assertTrue(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void analyzeEnvelopesTestWithPositiveIntValues(){
        envelope1 = new Envelope(7,10);
        envelope2 = new Envelope(3,4);
        assertTrue(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void analyzeEnvelopesTestWithEquelValues(){
        envelope1 = new Envelope(7,7);
        envelope2 = new Envelope(7,7);
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test
    public void analyzeEnvelopesTestWithDiagonal(){
        envelope1 = new Envelope(88,13);
        envelope2 = new Envelope(81,59);
        assertTrue(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void analyzeEnvelopesTestWithNegativeValues(){
        envelope1 = new Envelope(-9,-10);
        envelope2 = new Envelope(-7,-19);
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void analyzeEnvelopesTestWithZeroValues(){
        envelope1 = new Envelope(0,0);
        envelope2 = new Envelope(0,0);
        assertFalse(EnvelopeAnalyzer.analyzeEnvelopes(envelope1,envelope2));
    }
}