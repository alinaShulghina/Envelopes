package com.softserve.edu.envelope;

/**
 * Created by alin- on 09.12.2017.
 */
public class Envelope {

    private float minSide;
    private float maxSide;

    public Envelope(float sideA, float sideB) {
        if (sideA <= 0 | sideB <= 0) throw new IllegalArgumentException();
        this.minSide = Math.min(sideA, sideB);
        this.maxSide = Math.max(sideA, sideB);
    }

    public float getMinSide() {
        return minSide;
    }

    public float getMaxSide() {
        return maxSide;
    }

    @Override
    public String toString() {
        return "Envelope{" +
                "minSide=" + minSide +
                ", maxSide=" + maxSide +
                '}';
    }


}
