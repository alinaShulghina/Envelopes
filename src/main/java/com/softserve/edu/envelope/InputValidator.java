package com.softserve.edu.envelope;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

/**
 * Created by alin- on 09.12.2017.
 */
class InputValidator {
    /*
        method for user's input
     */
    public static float[] startInput() {
        float[] values = null;
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);
        try {
            System.out.print("Enter a first Envelop's sideA: ");
            float sideA1 = scanner.nextFloat();
            System.out.print("Enter a first Envelope's sideB: ");
            float sideB1 = scanner.nextFloat();
            System.out.print("Enter a second Envelop's sideA: ");
            float sideA2 = scanner.nextFloat();
            System.out.print("Enter a second Envelope's sideB: ");
            float sideB2 = scanner.nextFloat();
            if (sideA1 <= 0 | sideB1 <= 0 | sideA2 <= 0 | sideB2 <= 0) throw new IllegalArgumentException();
            values = new float[]{sideA1, sideB1, sideA2, sideB2};
        } catch (InputMismatchException e) {
            System.out.println("String is not allowed here!");
        } catch (IllegalArgumentException e) {
            System.out.println("Param must me more than zero!");
        }
        return values;
    }
}
