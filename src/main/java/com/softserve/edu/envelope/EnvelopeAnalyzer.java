package com.softserve.edu.envelope;

/*
    Class for analyzing two envelopes
 */
public class EnvelopeAnalyzer {
    /*
         Determine which envelope can be fit in another envelope:
         compare min sides of both envelopes and envelope with min value
         can be fit in second one.
         Then check if it can be fit in normally(without rotation). If not
         use Ford's formula for checking if it can be fit in by rotation
     */
    static boolean analyzeEnvelopes(Envelope envelope1, Envelope envelope2) {
        if (envelope1 == null | envelope2 == null) return false;
        float minEnvMaxSide; // min envelope max side
        float minEnvMinSide; // min envelope min side
        float maxEnvMaxSide; // max envelope max side
        float maxEnvMinSide; // max envelope min side
        if (envelope1.getMinSide() < envelope2.getMinSide()) {
            minEnvMaxSide = envelope1.getMaxSide();
            minEnvMinSide = envelope1.getMinSide();
            maxEnvMaxSide = envelope2.getMaxSide();
            maxEnvMinSide = envelope2.getMinSide();
        } else {
            minEnvMaxSide = envelope2.getMaxSide();
            minEnvMinSide = envelope2.getMinSide();
            maxEnvMaxSide = envelope1.getMaxSide();
            maxEnvMinSide = envelope1.getMinSide();
        }
        return (minEnvMaxSide < maxEnvMaxSide && minEnvMinSide < maxEnvMinSide)
                ^ fordFormula(minEnvMaxSide, minEnvMinSide, maxEnvMaxSide, maxEnvMinSide);
    }

    /*
        Ford's formula for checking if one envelope can be fit in another
        by rotation
     */
    private static boolean fordFormula(float minEnvMaxSide, float minEnvMinSide, float maxEnvMaxSide, float maxEnvMinSide) {
        float powP = (float) Math.pow(minEnvMaxSide, 2);
        float powQ = (float) Math.pow(minEnvMinSide, 2);
        float powA = (float) Math.pow(maxEnvMaxSide, 2);

        return (minEnvMaxSide > maxEnvMaxSide) && (maxEnvMinSide >= (
                ((2 * minEnvMaxSide * minEnvMinSide * maxEnvMaxSide) + (powP - powQ)
                        * Math.sqrt(powP + powQ - powA)) / (powP + powQ)));
    }


}
