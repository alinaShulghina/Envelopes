package com.softserve.edu.envelope;


import java.util.Locale;
import java.util.Scanner;

/**
 * Created by alin- on 09.12.2017.
 */
public class Main {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);
            float[] values = InputValidator.startInput();
            System.out.println("Can fit in one envelope to another: " +
                    EnvelopeAnalyzer.analyzeEnvelopes(new Envelope(values[0], values[1]),
                            new Envelope(values[2], values[3])));
            System.out.println("Do you want to exit? Type \"y\" or \"yes\" if so");
            String input = scanner.next().trim();
            if (input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")) {
                System.exit(0);
            }
        }
    }

}
